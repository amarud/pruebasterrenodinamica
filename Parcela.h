/*
 * Parcela.h
 *
 *  Created on: 20 may. 2018
 *      Author: patron
 */

#ifndef PARCELA_H_
#define PARCELA_H_

#include "Cultivo.h"
#include <iostream> // NULL pointer.

class Parcela {

private:

	bool ocupada;

	bool regada;

	Cultivo* tipoDeCultivo;

	ui tiempoDeCosecha;

	ui tiempoDeRecuperacion;




public:

	Parcela();

	bool estaOcupada();

	bool estaRegada();

	ui verTiempoDeCosecha();

	ui verTiempoDeRecuperacion();

	void setTiempoDeCosecha(ui tiempoDeCosecha);

	void setTiempoDeRecuperacion(ui tiempoDeRecuperacion);



};

#endif /* PARCELA_H_ */
