/*
 * principal.cpp
 *
 *  Created on: 21/5/2018
 *      Author: brian
 */
#include "Parcela.h"
#include "Terreno.h"
#include <iostream>

typedef unsigned int ui;

int main(){
	Parcela*** terreno = new Parcela**[5];
	for (ui i = 0; i < 4 ; i++){
		terreno[i] = new Parcela*[5];
		for (ui j = 0 ; j < 4 ; j++){
			terreno[i][j] = new Parcela();
		}
	}

	for (ui i = 0; i < 4 ; i++){
		for (ui j = 0 ; j < 4 ; j++){
			Parcela* parceAct = terreno[i][j];
			delete parceAct;
		}
		delete[] terreno[i];
	}
	delete[] terreno;

	return 0;
	}
