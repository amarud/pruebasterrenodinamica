/*
 * Parcela.cpp
 *
 *  Created on: 20 may. 2018
 *      Author: patron
 */

#include "Parcela.h"

Parcela::Parcela() {
	this->ocupada = false;
	this->regada = false;
	this->tipoDeCultivo = NULL;
	this->tiempoDeCosecha = 0;
	this->tiempoDeRecuperacion = 0;
}

bool Parcela::estaOcupada(){
	return this->ocupada;
}

bool Parcela::estaRegada(){
	return this->regada;
}

ui Parcela::verTiempoDeCosecha(){
	return this->tiempoDeCosecha;
}

ui Parcela::verTiempoDeRecuperacion(){
	return this->tiempoDeRecuperacion;
}

void Parcela::setTiempoDeCosecha(ui tiempoDeCosecha){
	this->tiempoDeCosecha = tiempoDeCosecha;
}

void Parcela::setTiempoDeRecuperacion(ui tiempoDeRecuperacion){
	this->tiempoDeCosecha = tiempoDeRecuperacion;
}

