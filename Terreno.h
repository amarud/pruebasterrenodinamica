/*
 * Terreno.h
 *
 *  Created on: 16/05/2018
 *      Author: patron
 */

#ifndef TERRENO_H_
#define TERRENO_H_

//#include "CondicionesIniciales.h"
#include "Parcela.h"
//#include "Lista.h"

//#define NULL 0

class Terreno {
private:
	Parcela*** terreno;
	ui alto;
	ui ancho;


	Parcela*** inicializarTerreno(const ui alto,const ui ancho);

public:
	Terreno(ui alto, ui ancho);

	ui verAlto();

	ui verAncho();

	ui calcularValorTerreno();

	~Terreno();

};

#endif /* TERRENO_H_ */
