/*
 * Terreno.cpp
 *
 *  Created on: 16/05/2018
 *      Author: patron
 */

#include "Terreno.h"


Parcela*** Terreno::inicializarTerreno(const ui alto,const ui ancho){
	Parcela*** terreno = new Parcela**[ancho +  1];
		for (ui i = 0; i < alto ; i++){
			terreno[i] = new Parcela*[alto + 1];
			for (ui j = 0 ; j < ancho ; j++){
				terreno[i][j] = new Parcela();
			}
		}

	return terreno; //terreno se accede de la forma Parcela* parcelaAct = terreno[i][j];
}


Terreno::Terreno(ui alto, ui ancho) {
	this->terreno = inicializarTerreno(alto, ancho);
	this->alto = alto;
	this->ancho = ancho;
}

ui Terreno::verAlto(){
	return this->alto;
}

ui Terreno::verAncho(){
	return this->ancho;
}

Terreno::~Terreno() {
	Parcela*** terreno = this->terreno;

	for (ui i = 0; i < this->verAlto() ; i++){
			for (ui j = 0 ; j < this->verAncho() ; j++){
				Parcela* parceAct = terreno[i][j];
				delete parceAct;
			}
			delete[] terreno[i];
		}
		delete[] terreno;


}

